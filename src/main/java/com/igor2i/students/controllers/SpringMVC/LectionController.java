package com.igor2i.students.controllers.SpringMVC;

import com.igor2i.students.modules.pojo.university.objects.Lection;
import com.igor2i.students.services.LectionsService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import java.sql.Timestamp;

/**
 * Created by igor2i on 10.03.17.
 */
@Controller
public class LectionController {

    private static final Logger LOGGER = LogManager.getLogger(LectionController.class);

    @Autowired
    private LectionsService lectionsService;


    @RequestMapping(value = "/lections", method = RequestMethod.GET)
    public ModelAndView registration() {

        ModelAndView modelAndView = new ModelAndView();

        LOGGER.debug("getLections");
        modelAndView.addObject("lections", lectionsService.getAllLections());

        modelAndView.setViewName("lections");
        return modelAndView;
    }

    @RequestMapping(value = "/lections", method = RequestMethod.POST)
    protected ModelAndView registrationPost(
            @RequestParam(value = "type", required = false) String typeAction,
            @RequestParam(value = "id", required = false) Integer idLection) {

        ModelAndView modelAndView = new ModelAndView();

        LOGGER.debug("postLections " + typeAction);

        if ("del".equals(typeAction)) {

            LOGGER.debug("post del" + idLection);
            lectionsService.delLection(idLection);

            modelAndView.setViewName("redirect: lection");
        } else if ("add".equals(typeAction)) {

            LOGGER.debug("post add");
            modelAndView.setViewName("redirect: editLection");

        } else if ("edit".equals(typeAction)) {

            LOGGER.debug("post edit " + idLection);

            modelAndView.setViewName("redirect: /editLection?id=" + idLection);
        }
        return modelAndView;
    }

    @RequestMapping(value = "/editLection", method = RequestMethod.GET)
    protected ModelAndView editLectionGet(@RequestParam(value = "id", required = false) String idStr) {

        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("editLection");

        LOGGER.debug(idStr);
        if (idStr != null) {
            LOGGER.debug("edit lection " + idStr);
            Lection lection = lectionsService.getById(new Integer(idStr));
            if (lection != null) {
                LOGGER.debug("edit lection " + idStr);
                modelAndView.addObject("title", "edit lection " + lection.getName());
                modelAndView.addObject("id", lection.getId());
                modelAndView.addObject("name", lection.getName());
                modelAndView.addObject("text", lection.getText());
                modelAndView.addObject("subject", lection.getSubject());
                modelAndView.addObject("dateTime", lection.getDateTime());

            }

            return modelAndView;

        } else {

            modelAndView.addObject("title", "Add new lection");

            return modelAndView;
        }
    }


    @RequestMapping(value = "/editLection", method = RequestMethod.POST)
    public ModelAndView editLectionPost(@RequestParam(value = "id", required = false) String idStr,
                                        @RequestParam(value = "name", required = false) String name,
                                        @RequestParam(value = "text", required = false) String text,
                                        @RequestParam(value = "subject", required = false) String subject,
                                        @RequestParam(value = "dateTime", required = false) String dateTime) {

        String dateTimeTest = dateTime.replace("T", " ").concat(":00");
        Lection lection = new Lection(name, text, subject, Timestamp.valueOf(dateTimeTest));

        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("lections");

        if (idStr != null) {

            lectionsService.updateLection(new Integer(idStr), lection);

        } else {

            lectionsService.newLection(lection);

        }
        return modelAndView;
    }


}
