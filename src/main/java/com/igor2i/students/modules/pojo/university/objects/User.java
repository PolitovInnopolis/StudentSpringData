package com.igor2i.students.modules.pojo.university.objects;

import org.springframework.stereotype.Component;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Component
@Entity(name = "users")
public class User {

  @Id
  @GeneratedValue
  private Long id;
  @Column(unique = true)
  private String login;
  @Column
  private String password;

  public User(User user) {
    this.id = user.getId();
    this.login = user.getLogin();
    this.password = user.getPassword();

  }

  public User() {
  }

  public User(Long id, String user, String password) {
    this.id = id;
    this.login = user;
    this.password = password;
  }

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public String getLogin() {
    return login;
  }

  public void setLogin(String login) {
    this.login = login;
  }

  public String getPassword() {
    return password;
  }

  public void setPassword(String password) {
    this.password = password;
  }

  @Override
  public String toString() {
    return "User{" +
            "id=" + id +
            ", user='" + login + '\'' +
            ", password='" + password + '\'' +
            '}';
  }
}
