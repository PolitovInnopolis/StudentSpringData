package com.igor2i.students.modules.pojo.university;

import com.igor2i.students.modules.pojo.university.objects.Role;
import org.springframework.stereotype.Component;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

/**
 * Created by igor2i on 13.03.17.
 */
@Component
public class Roles implements WorkWithDB<Role> {
    @Override
    public ArrayList<Role> getAll() throws SQLException {
        return null;
    }

    @Override
    public Role getById(int id) throws SQLException {
        return null;
    }

    @Override
    public Role getByName(String name) throws SQLException {
        Connection connection = ConnectionsMyDB.getDbCon().connection;

        String query = "select u.login, au.role from users u, roles au where u.login = au.login and u.login = ?";
        PreparedStatement preparedStatement = connection.prepareStatement(query);
        preparedStatement.setString(1, String.valueOf(name));

        ResultSet resultSet = preparedStatement.executeQuery();
        Role role = null;

        while (resultSet.next()) {
            role = new Role(
                    resultSet.getString("login"),
                    resultSet.getString("role")
            );
        }
        return role;
    }

    @Override
    public void setNewColumn(Role object) throws SQLException {

    }

    @Override
    public void setUpdateById(int id, Role object) throws SQLException {

    }
}
