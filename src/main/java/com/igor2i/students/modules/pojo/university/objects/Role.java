package com.igor2i.students.modules.pojo.university.objects;

/**
 * Created by igor2i on 13.03.17.
 */
public class Role {

    private String login;
    private String role;

    public Role(Role role) {
        this.login = role.getLogin();
        this.role = role.getRole();
    }

    public Role() {
    }

    public Role(String login, String role) {
        this.login = login;
        this.role = role;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }
}
