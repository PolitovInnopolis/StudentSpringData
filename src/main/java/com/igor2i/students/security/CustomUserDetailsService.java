package com.igor2i.students.security;

import com.igor2i.students.modules.pojo.university.objects.User;
import com.igor2i.students.services.RoleService;
import com.igor2i.students.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.LinkedList;
import java.util.List;

/**
 * Created by igor2i on 09.03.17.
 */
@Service("customUserDetailsService")
public class CustomUserDetailsService implements UserDetailsService {

    private final UserService userService;
    private final RoleService roleService;

    @Autowired
    public CustomUserDetailsService(UserService userService, RoleService roleService) {
        this.userService = userService;
        this.roleService = roleService;
    }

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {

        User user = userService.getByName(username);

        if (user == null) {
            throw new UsernameNotFoundException("User '" + username + "' not found");
        } else {

            List<GrantedAuthority> roles = new LinkedList<>();
            roles.add(new SimpleGrantedAuthority(roleService.getByName(username).getRole()));

            CustomUserDetails customUserDetails = new CustomUserDetails(user, roles);
            return customUserDetails;
        }
    }
}
